from django.conf.urls import url,include
from django.contrib import admin

from rest_framework.routers import DefaultRouter

from exercises.viewsets import ExerciseViewSet, MuscleActivationViewSet
from workouts.viewsets import WorkoutViewSet,ExerciseRelationshipViewSet
from persons.viewsets import PersonViewSet,UserViewSet,MuscleProgressionViewSet,MyProfile
from userworkouts.viewsets import UserWorkoutsViewSet, UserExerciseRelationshipViewSet
from muscles.viewsets import MuscleViewSet
from exerciseprogress.viewsets import ExerciseProgressionViewSet
from userworkouts_done.viewsets import UserWorkoutsDoneViewSet

router = DefaultRouter()
router.register(r'exercises', ExerciseViewSet)
router.register(r'muscles', MuscleViewSet)
router.register(r'workouts', WorkoutViewSet)
router.register(r'users', UserViewSet)
router.register(r'persons', PersonViewSet)
router.register(r'me', MyProfile,base_name='me')
router.register(r'userworkouts', UserWorkoutsViewSet)
router.register(r'userworkoutsreps', UserExerciseRelationshipViewSet)
router.register(r'userworkoutsdone', UserWorkoutsDoneViewSet)
router.register(r'exerciseprogressions', ExerciseProgressionViewSet,base_name='exerciseprogressions')
router.register(r'muscleprogressions', MuscleProgressionViewSet,base_name='muscleprogressions')


urlpatterns = [
    url(r'^v1/', include(router.urls)),
    url(r'^auth/', include('rest_framework_social_oauth2.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]
