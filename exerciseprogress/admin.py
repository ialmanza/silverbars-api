from django.contrib import admin

# Register your models here.
from .models import ExerciseProgression

class ExerciseProgressionAdmin(admin.ModelAdmin):
	list_display = ('id','date','person','my_workout_done','exercise','total_repetition',
		'repetitions_done','total_seconds','seconds_done','total_weight')


admin.site.register(ExerciseProgression, ExerciseProgressionAdmin)
