from rest_framework import serializers

from exerciseprogress.models import ExerciseProgression
from persons.models import Person
from exercises.models import Exercise
from userworkouts_done.models import UserWorkoutDone

from exercises.serializers import ExerciseSerializer
from userworkouts_done.serializers import UserWorkoutsDoneSerializer

class ExerciseProgressionSerializer(serializers.HyperlinkedModelSerializer):
    my_workout_done = UserWorkoutsDoneSerializer(read_only=True,)
    my_workout_done_id = serializers.PrimaryKeyRelatedField(queryset=UserWorkoutDone.objects.all(), write_only=True, source='my_workout_done',)
    person = serializers.PrimaryKeyRelatedField(read_only=False, queryset=Person.objects.all())
    exercise = ExerciseSerializer(read_only=True,)
    exercise_id = serializers.PrimaryKeyRelatedField(queryset=Exercise.objects.all(), write_only=True, source='exercise',)
    total_time = serializers.DecimalField(max_digits=6, decimal_places=2,coerce_to_string=False)
    total_weight = serializers.DecimalField(max_digits=4, decimal_places=2,coerce_to_string=False)
    date = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S")

    class Meta:
        model = ExerciseProgression
        fields = ('id','my_workout_done','my_workout_done_id','person','total_time','exercise','exercise_id',
            'total_repetition','repetitions_done','total_seconds','seconds_done','total_weight','date')


