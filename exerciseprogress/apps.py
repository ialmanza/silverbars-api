from django.apps import AppConfig


class ExerciseprogressConfig(AppConfig):
    name = 'exerciseprogress'
