from __future__ import unicode_literals

from django.db import models

from persons.models import Person
from exercises.models import Exercise
from userworkouts_done.models import UserWorkoutDone

def cero_default():
        return 0

class ExerciseProgression(models.Model):
	date = models.DateTimeField()
	person = models.ForeignKey(Person,related_name='exercises_progression',)
	my_workout_done = models.ForeignKey(UserWorkoutDone)
	exercise = models.ForeignKey(Exercise)
	total_time = models.DecimalField(max_digits=6,decimal_places=2)
	total_repetition = models.PositiveIntegerField()
	repetitions_done = models.PositiveIntegerField()
	total_seconds = models.PositiveIntegerField()
	seconds_done = models.PositiveIntegerField()
	total_weight = models.DecimalField(max_digits=4,decimal_places=2)


