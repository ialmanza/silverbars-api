from rest_framework import viewsets
from rest_framework.permissions import IsAdminUser, IsAuthenticated

from .models import ExerciseProgression

from .serializers import ExerciseProgressionSerializer


class ExerciseProgressionViewSet(viewsets.ModelViewSet):
    serializer_class = ExerciseProgressionSerializer

    def get_queryset(self):
        user = self.request.user
        return ExerciseProgression.objects.filter(person__user__username=user)
       
