from rest_framework import serializers

from .models import Muscle

# Serializers define the API representation.
class MuscleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Muscle
        fields = ('id','muscle_name',)


