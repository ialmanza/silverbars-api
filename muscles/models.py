from __future__ import unicode_literals

#Imports from django core
from django.db import models


class Muscle(models.Model):
	muscle_name = models.CharField(max_length=120)

	def __str__(self):
		return '%s' % (self.muscle_name)