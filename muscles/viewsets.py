from rest_framework import viewsets

#Imports from your apps
from .serializers import MuscleSerializer
from .models import Muscle

class MuscleViewSet(viewsets.ModelViewSet):
    queryset = Muscle.objects.all()
    serializer_class = MuscleSerializer