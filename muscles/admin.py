from django.contrib import admin
from .models import Muscle


class MusclesAdmin(admin.ModelAdmin):
	list_display = ('id','muscle_name',)

admin.site.register(Muscle, MusclesAdmin)