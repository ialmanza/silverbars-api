from rest_framework import viewsets,generics,authentication, permissions,mixins

from rest_framework.permissions import IsAdminUser,IsAuthenticated
from rest_framework.decorators import api_view, permission_classes 

from django.contrib.auth.models import User

from .serializers import PersonSerializer,ProgressionRelationshipSerializer,UserSerializer
from .models import Person, ProgressionRelationship

from .permissions import IsOwnerOrReadOnly
from .filters import IsOwnerFilterBackend,IsOwnerFilterBackend2


class MyProfile(viewsets.ModelViewSet):
    serializer_class = PersonSerializer
    queryset = Person.objects.all()
    filter_backends = (IsOwnerFilterBackend2,)


class MuscleProgressionViewSet(viewsets.ModelViewSet):
    serializer_class = ProgressionRelationshipSerializer
    queryset = ProgressionRelationship.objects.all()
    filter_backends = (IsOwnerFilterBackend,)


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAdminUser,)

class PersonViewSet(viewsets.ModelViewSet):
    queryset = Person.objects.all()
    serializer_class = PersonSerializer
    permission_classes = (IsAdminUser,)

   