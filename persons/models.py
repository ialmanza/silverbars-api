from __future__ import unicode_literals

#Imports from django core
from django.db import models
from django.contrib.auth.models import User

# Imports from your apps
from muscles.models import Muscle


class Person(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE,unique=True)
    muscles = models.ManyToManyField(Muscle,through='ProgressionRelationship',blank=True)
    age = models.PositiveIntegerField(null=True,blank=True)

    def __str__(self):
        return '%s' % (self.user.username)

class ProgressionRelationship(models.Model):
    muscle = models.ForeignKey(Muscle)
    person = models.ForeignKey(Person)

    muscle_activation_progress = models.PositiveIntegerField()
    level = models.PositiveIntegerField()
    date = models.DateTimeField()
