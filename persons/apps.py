from __future__ import unicode_literals

from django.apps import AppConfig

class PersonsConfig(AppConfig):
    name = 'persons'
    verbose_name = 'Person'

    def ready(self):
        import persons.signals  # noqa
      
