from rest_framework import serializers

from django.contrib.auth.models import User
from userworkouts.models import UserWorkout
from muscles.models import Muscle
from .models import Person, ProgressionRelationship

from userworkouts.serializers import UserWorkoutsSerializer
from muscles.serializers import MuscleSerializer


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('id','username','first_name','last_name','email',)
        

class ProgressionRelationshipSerializer(serializers.HyperlinkedModelSerializer):
    muscle = MuscleSerializer(read_only=True)
    muscle_id = serializers.PrimaryKeyRelatedField(queryset=Muscle.objects.all(), write_only=True, source='muscle',)
    person = serializers.PrimaryKeyRelatedField(read_only=False,queryset=Person.objects.all())
    date  = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S")
    
    class Meta:
        model = ProgressionRelationship
        fields = ('id','muscle','muscle_id','muscle_activation_progress','level','date','person',)

class PersonSerializer(serializers.HyperlinkedModelSerializer):
    user = UserSerializer()
    muscles_progression = ProgressionRelationshipSerializer(source='progressionrelationship_set', many=True)

    class Meta:
        model = Person
        fields = ('id','user','age','my_workouts','my_workouts_done','exercises_progression','muscles_progression',)






