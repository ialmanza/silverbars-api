from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

from persons.models import Person
from userworkouts.models import UserWorkout
from exerciseprogress.models import ExerciseProgression

class ExerciseProgressInline(admin.TabularInline):
    model = ExerciseProgression


class MuscleProgressInline(admin.TabularInline):
    model = Person.muscles.through


class PersonAdmin(admin.ModelAdmin):
    inlines = [MuscleProgressInline,ExerciseProgressInline]
    search_fields = ('first_name', 'last_name')
    list_display = ('person_name','age','muscles_progression','exercises_progression')

    def muscles_progression(self, obj):
        return ", \n".join([p.muscle_name for p in obj.muscles.all()])

    def exercises_progression(self, obj):
        return ", \n".join([exercise.exercise.exercise_name for exercise in obj.exercises_progression.all()])

    def person_name(self, object):
        return object.user.username
  

class PersonInline(admin.StackedInline):
    model = Person
    can_delete = False
    verbose_name_plural = 'person'
 
    
# Define a new User admin
class UserAdmin(BaseUserAdmin):
    inlines = (PersonInline, )

# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)

admin.site.register(Person ,PersonAdmin)
