from django.contrib.auth.models import User

from django.dispatch import receiver
from django.db.models.signals import post_save

from persons.models import Person


@receiver(post_save, sender=User)
def create_user_person(sender, instance, created, **kwargs):
    if created:
        Person.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_person(sender, instance, **kwargs):
    instance.person.save()