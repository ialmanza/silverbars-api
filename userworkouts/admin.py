from django.contrib import admin

from .models import UserWorkout,UserExerciseRelationship

class ExerciseInline(admin.TabularInline):
    model = UserWorkout.exercises.through


class UserWorkoutAdmin(admin.ModelAdmin):
	list_display = ('person','workout_name','level','workout_image','sets','main_muscle')
	inlines = [
        ExerciseInline,
    ]
	exclude = ('exercises',)

class UserExerciseRelationshipAdmin(admin.ModelAdmin):
	list_display = ('exercise','workout','repetition',)


admin.site.register(UserWorkout, UserWorkoutAdmin)
admin.site.register(UserExerciseRelationship, UserExerciseRelationshipAdmin)