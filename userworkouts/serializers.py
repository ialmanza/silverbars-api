from rest_framework import serializers

from userworkouts.models import UserWorkout,UserExerciseRelationship
from persons.models import Person
from exercises.models import Exercise

from exercises.serializers import ExerciseSerializer


class UserExerciseRelationshipSerializer(serializers.HyperlinkedModelSerializer):
    exercise = ExerciseSerializer(read_only=True)
    exercise_id = serializers.PrimaryKeyRelatedField(queryset=Exercise.objects.all(), write_only=True, source='exercise',)
    workout = serializers.PrimaryKeyRelatedField(read_only=False, queryset=UserWorkout.objects.all())
    
    class Meta:
        model = UserExerciseRelationship
        fields = ('id','workout','exercise','exercise_id','repetition','seconds',)



class UserWorkoutsSerializer(serializers.HyperlinkedModelSerializer):
    person = serializers.PrimaryKeyRelatedField(read_only=False, queryset=Person.objects.all())
    exercises = UserExerciseRelationshipSerializer(source='userexerciserelationship_set', many=True)

    class Meta:
        model = UserWorkout
        fields = ('id','workout_name','workout_image', 'sets','level','main_muscle','exercises','person',)





