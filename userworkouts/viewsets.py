# Import rest core 
from rest_framework import viewsets,authentication, permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAdminUser, IsAuthenticated

# Import from your apps

from userworkouts.models import UserWorkout,UserExerciseRelationship

from .serializers import UserWorkoutsSerializer,UserExerciseRelationshipSerializer

class UserWorkoutsViewSet(viewsets.ModelViewSet):
    queryset = UserWorkout.objects.all()
    serializer_class = UserWorkoutsSerializer

    def get_queryset(self):
        user = self.request.user
        return UserWorkout.objects.filter(person__user__username=user)


class UserExerciseRelationshipViewSet(viewsets.ModelViewSet):
    queryset = UserExerciseRelationship.objects.all()
    serializer_class = UserExerciseRelationshipSerializer


