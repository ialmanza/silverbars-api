from fabric.api import env, task, run,cd,sudo
from fabric.contrib.project import rsync_project
from fabric.contrib.files import exists
from fabvenv import virtualenv

USER = 'isaac'  
HOST = '104.131.182.62'  # server SSH address  
APP_NAME = 'silverbars'

env.hosts = ['{}@{}'.format(USER, HOST)]
env.password = "booth1319"


def run_migrations():
    run('ls')
    with virtualenv('/home/isaac/env'):
        run('pip freeze')

        with cd('/home/isaac/silverbars'):
            run('./manage.py makemigrations')
            run('./manage.py migrate')


def run_migrations_initial():
    run('ls')
    with virtualenv('/home/isaac/env'):
        run('pip freeze')

        with cd('/home/isaac/silverbars'):
            run('./manage.py makemigrations')
            run('./manage.py migrate --fake-initial')



def deploy_static():
    with virtualenv('/home/isaac/env'):
        with cd('/home/isaac/silverbars'):
            run('./manage.py collectstatic --noinput')
            
            #run('gunicorn silverbars.wsgi:application --bind 127.0.0.1:8000')

def test_gunicorn():
    with virtualenv('/home/isaac/env'):
        with cd('/home/isaac/silverbars'):
            run('gunicorn silverbars.wsgi:application --bind 127.0.0.1:8000')

def syn_files():
    rsync_project(local_dir='/Users/isaacalmanza/documents/backend/silverbars', remote_dir='/home/isaac/',
                      exclude=['.git'])

def restart_nginx():
    sudo('service nginx restart')
   

def restart_supervisor():
    sudo('supervisorctl status')
    sudo('supervisorctl reload')
    #run('cat /home/isaac/silverbars/log/supervisor.log')
    

def test_sentry():
    with virtualenv('/home/isaac/env'):
        with cd('/home/isaac/silverbars'):
            run('./manage.py raven test')
    

@task(default=True)
def deploy():

    syn_files()
    #run_migrations()
    #run_migrations()
    
    #test_gunicorn()

    restart_supervisor()
    restart_nginx()
    



    