from django.apps import AppConfig


class UserworkoutsDoneConfig(AppConfig):
    name = 'userworkouts_done'
