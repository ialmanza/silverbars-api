from rest_framework import serializers

from userworkouts.models import UserWorkout
from persons.models import Person
from .models import UserWorkoutDone

from userworkouts.serializers import UserWorkoutsSerializer


class UserWorkoutsDoneSerializer(serializers.HyperlinkedModelSerializer):
    my_workout = UserWorkoutsSerializer(read_only=True)
    my_workout_id = serializers.PrimaryKeyRelatedField(queryset=UserWorkout.objects.all(), write_only=True, source='my_workout',)
    person  = serializers.PrimaryKeyRelatedField(read_only=False, queryset=Person.objects.all())
    total_time = serializers.DecimalField(max_digits=6, decimal_places=2,coerce_to_string=False)
    date = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S")

    class Meta:
        model = UserWorkoutDone
        fields = ('id','date','my_workout','my_workout_id','person','total_time','sets_completed',)


