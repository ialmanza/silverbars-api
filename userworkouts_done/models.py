from __future__ import unicode_literals

from django.db import models

from persons.models import Person
from userworkouts.models import UserWorkout

class UserWorkoutDone(models.Model):
    person = models.ForeignKey(Person,related_name='my_workouts_done',)
    my_workout = models.ForeignKey(UserWorkout)
    total_time = models.DecimalField(max_digits=6,decimal_places=2)
    sets_completed =  models.PositiveIntegerField()
    date = models.DateTimeField()

    def __str__(self):
        return '%s' % (self.my_workout)


    