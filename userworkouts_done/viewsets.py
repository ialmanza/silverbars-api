from rest_framework import viewsets
from rest_framework.permissions import IsAdminUser, IsAuthenticated

from .models import UserWorkoutDone

from .serializers import UserWorkoutsDoneSerializer

class UserWorkoutsDoneViewSet(viewsets.ModelViewSet):
    queryset = UserWorkoutDone.objects.all()
    serializer_class = UserWorkoutsDoneSerializer
    
    def get_queryset(self):
        user = self.request.user
        return UserWorkoutDone.objects.filter(person__user__username=user)