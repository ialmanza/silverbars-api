from django.contrib import admin

# Register your models here.
from .models import UserWorkoutDone

class UserWorkoutDoneAdmin(admin.ModelAdmin):
	list_display = ('date','person','my_workout','total_time','sets_completed')

admin.site.register(UserWorkoutDone, UserWorkoutDoneAdmin)
