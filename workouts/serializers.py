from rest_framework import serializers

from .models import Workout, ExerciseRelationship
from exercises.serializers import ExerciseSerializer

class ExerciseRelationshipSerializer(serializers.ModelSerializer):
    exercise = ExerciseSerializer()
    
    class Meta:
        model = ExerciseRelationship
        fields = ('exercise','repetition','seconds')



class WorkoutSerializer(serializers.ModelSerializer):
    exercises = ExerciseRelationshipSerializer(source='exerciserelationship_set', many=True)

    class Meta:
        model = Workout
        fields = ('id','workout_name','workout_image', 'sets','level','main_muscle','exercises',)
        



