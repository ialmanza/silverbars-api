from rest_framework import viewsets,generics
from rest_framework import filters

# Third-party app imports
from rest_framework.permissions import IsAuthenticated
from oauth2_provider.ext.rest_framework import TokenHasReadWriteScope, TokenHasScope

# Import from your apps
from .serializers import WorkoutSerializer,ExerciseRelationshipSerializer
from .models import Workout,ExerciseRelationship


class WorkoutViewSet(viewsets.ModelViewSet):
    queryset = Workout.objects.all()
    serializer_class = WorkoutSerializer
    permission_classes = (IsAuthenticated,TokenHasReadWriteScope)

class ExerciseRelationshipViewSet(viewsets.ModelViewSet):
    queryset = ExerciseRelationship.objects.all()
    serializer_class = ExerciseRelationshipSerializer
    permission_classes = (IsAuthenticated,TokenHasReadWriteScope)
