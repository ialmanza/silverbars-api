from __future__ import unicode_literals

from django.db import models
from exercises.models import Exercise


class Workout(models.Model):
    workout_name =  models.CharField(max_length=120)
    workout_image = models.ImageField(upload_to='workouts/images/',blank=True)
    sets = models.PositiveIntegerField()
    exercises = models.ManyToManyField(Exercise,through='ExerciseRelationship',)

    LEVEL = (
        ('EASY', 'EASY'),
        ('NORMAL', 'NORMAL'),
        ('HARD', 'HARD'),
        ('CHALLENGING', 'CHALLENGING'),
    )
    MAIN_MUSCLES = (
        ('ABS/CORE','ABS/CORE'),
        ('UPPER BODY','UPPER BODY'),
        ('LOWER BODY','LOWER BODY'),
        ('FULL BODY','FULL BODY'),
    )
    level = models.CharField(max_length=30, choices=LEVEL)
    main_muscle = models.CharField(max_length=30, choices=MAIN_MUSCLES)
    
    def __str__(self):
        return '%s' % (self.workout_name)


def cero_default():
        return 0

class ExerciseRelationship(models.Model):
    exercise = models.ForeignKey(Exercise)
    workout = models.ForeignKey(Workout)
    repetition = models.PositiveIntegerField(default=cero_default)
    seconds = models.PositiveIntegerField(default=cero_default)
    
    


