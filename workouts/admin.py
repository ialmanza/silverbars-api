from django.contrib import admin

from .models import Workout,ExerciseRelationship

class ExerciseInline(admin.TabularInline):
    model = Workout.exercises.through


class WorkoutAdmin(admin.ModelAdmin):
	list_display = ('workout_name','level','workout_image','sets','main_muscle')
	inlines = [
        ExerciseInline,
    ]
	exclude = ('exercises',)

class ExerciseRelationshipAdmin(admin.ModelAdmin):
	list_display = ('exercise','workout','repetition',)


admin.site.register(Workout, WorkoutAdmin)
admin.site.register(ExerciseRelationship, ExerciseRelationshipAdmin)