from django.contrib import admin
from .models import ExerciseType


class ExerciseTypeAdmin(admin.ModelAdmin):
	list_display = ('type_exercise',)


admin.site.register(ExerciseType, ExerciseTypeAdmin)
