from __future__ import unicode_literals

from django.db import models

class ExerciseType(models.Model):
	type_exercise = models.CharField(max_length=30)

	def __str__(self):
		return '%s' % (self.type_exercise)