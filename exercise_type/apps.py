from __future__ import unicode_literals

from django.apps import AppConfig


class ExerciseTypeConfig(AppConfig):
    name = 'exercise_type'
