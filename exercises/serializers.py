from rest_framework import serializers

from .models import Exercise, MuscleRelationship
from muscles.serializers import MuscleSerializer


class MuscleRelationshipSerializer(serializers.ModelSerializer):
    muscle = serializers.StringRelatedField()
    
    class Meta:
        model = MuscleRelationship
        fields = ('muscle','muscle_activation','classification')
       

class ExerciseSerializer(serializers.HyperlinkedModelSerializer):
    type_exercise = serializers.StringRelatedField(many=True)
    muscles = MuscleRelationshipSerializer(source='musclerelationship_set',many=True)
    exercise_audio = serializers.FileField(allow_null=True)
    exercise_image = serializers.ImageField(allow_null=True)


    class Meta:
        model = Exercise
        fields = ('id','exercise_name','level','type_exercise','muscles','exercise_audio','exercise_image',)
       