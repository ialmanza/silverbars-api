from django.contrib import admin

from .models import Exercise,MuscleRelationship


class MusclesInline(admin.TabularInline):
    model = Exercise.muscles.through

class ExerciseTypeInline(admin.TabularInline):
	model = Exercise.type_exercise.through
	    

class ExerciseAdmin(admin.ModelAdmin):
	list_display = ('id','exercise_name','level','exercise_image','exercise_audio',)
	inlines = [
        MusclesInline,
        ExerciseTypeInline
    ]
	exclude = ('muscles','type_exercise',)

class MuscleRelationshipAdmin(admin.ModelAdmin):
	list_display = ('muscle','muscle_activation',)



admin.site.register(Exercise, ExerciseAdmin)
admin.site.register(MuscleRelationship, MuscleRelationshipAdmin)