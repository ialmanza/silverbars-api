from rest_framework import viewsets,generics

# Third-party app imports
from rest_framework.permissions import IsAuthenticated
from oauth2_provider.ext.rest_framework import TokenHasReadWriteScope, TokenHasScope

# Imports from your apps
from .serializers import ExerciseSerializer, MuscleRelationshipSerializer
from .models import Exercise, MuscleRelationship
from rest_framework import filters

from persons.permissions import IsAdminOrReadOnly


class ExerciseViewSet(viewsets.ModelViewSet):
    queryset = Exercise.objects.all()
    serializer_class = ExerciseSerializer
    permission_classes = (IsAdminOrReadOnly,)



class MuscleActivationViewSet(viewsets.ModelViewSet):
    queryset = MuscleRelationship.objects.all()
    serializer_class = MuscleRelationshipSerializer
