from __future__ import unicode_literals

#Imports from django core
from django.db import models
from django.utils.encoding import python_2_unicode_compatible

#Imports from your apps
from muscles.models import Muscle
from exercise_type.models import ExerciseType


class Exercise(models.Model):
    exercise_name =  models.CharField(max_length=120)
    exercise_image = models.ImageField(upload_to='exercises/images/',blank=True)
    exercise_audio = models.FileField(upload_to='exercises/audio/',blank=True)

    def __str__(self):
        return '%s' % (self.exercise_name)
  
    LEVEL = (
        ('EASY', 'EASY'),
        ('NORMAL', 'NORMAL'),
        ('HARD', 'HARD'),
        ('CHALLENGING', 'CHALLENGING'),
    )

    type_exercise = models.ManyToManyField(ExerciseType)
    level = models.CharField(max_length=90, choices=LEVEL)
    muscles = models.ManyToManyField(Muscle,through='MuscleRelationship',)


class MuscleRelationship(models.Model):
    muscle = models.ForeignKey(Muscle,on_delete=models.CASCADE)
    exercise = models.ForeignKey(Exercise,on_delete=models.CASCADE)
    muscle_activation = models.PositiveIntegerField()

    CLASSIF = (
        ('Target', 'Target'),
        ('Synergists', 'Synergists'),
        ('Dynamic Stabilizers', 'Dynamic Stabilizers'),
    )
    classification = models.CharField(max_length=90, choices=CLASSIF)



    


    






   